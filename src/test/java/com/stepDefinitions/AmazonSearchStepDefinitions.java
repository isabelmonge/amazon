package com.stepDefinitions;

import com.serenitySteps.AmazonSearchSteps;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class AmazonSearchStepDefinitions {

    @Steps
    AmazonSearchSteps amazonSearchSteps;

    @Given("I want to search in Amazon")
    public void iWantToSearchInAmazon() {
        amazonSearchSteps.openAmazonSearchPage();
    }

    @When("I search for '(.*)'")
    public void iSearchFor(String searchRequest) {
        amazonSearchSteps.searchFor(searchRequest);
    }

    @Then("I should see link to '(.*)'")
    public void iShouldSeeLinkTo(String searchResult) {
        amazonSearchSteps.verifyResult(searchResult);
    }


}
