package com.stepDefinitions;

import com.serenitySteps.AmazonLoginSteps;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class AmazonLoginStepDefinition {

    @Steps
    AmazonLoginSteps amazonLoginSteps;



    @Given("^I am on Login Amazon Page$")
    public void iAmOnLoginAmazonPage() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @When("^I fill '<email>' with \"([^\"]*)\" and click on 'Continuar' button$")
    public void iFillEmailWithAndClickOnContinuarButton(String email) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^I should see url contains \"([^\"]*)\"$")
    public void iShouldSeeUrlContains(String url) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^I should see my Dashboard page$")
    public void iShouldSeeMyDashboardPage() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^I fill  password with \"([^\"]*)\" and$")
    public void iFillPasswordWithAnd(String password) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^I click on \"([^\"]*)\" button$")
    public void iClickOnButton(String nameButton) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}
