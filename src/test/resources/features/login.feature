@login
Feature: Amazon Login
  In order to login
  As a generic user
  I should see profile page

  Scenario: Amazon Login
    Given I am on Login Amazon Page

    When  I fill '<email>' with "isabel.monge@alten.es" and click on 'Continuar' button
    And  I fill  password with "<password>" and
    And I click on "Iniciar sesion" button
    Then I should see url contains "yourstore"
    And I should see my Dashboard page

  Examples:
  | email  | password  |
  | email1 | password1 |
  | email2 | password2 |

