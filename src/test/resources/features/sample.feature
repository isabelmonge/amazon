@search
Feature: Amazon Search
  In order to find items
  As a generic user
  I want to be able to search with Amazon

#  Background:
#    Given I am on Login Amazon Page

  Scenario: Amazon Search
    Given I want to search in Amazon
    When I search for 'Falda'
    Then I should see link to 'FIND Falda Metálica Plisada para Mujer'

  Scenario Outline: Amazon Search multiple
    Given I want to search in Amazon
    When I search for '<search_request>'
    Then I should see link to '<search_result>'
    Examples:
      | search_request | search_result                                                                       |
      | kindle         | aaaaa                                                                               |
      | Cucumber       | The Cucumber for Java Book: Behaviour-Driven Development for Testers and Developers |

