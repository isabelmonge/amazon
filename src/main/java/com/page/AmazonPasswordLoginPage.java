package com.page;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.WhenPageOpens;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;

@DefaultUrl("https://www.amazon.es/ap/signin")


public class AmazonPasswordLoginPage extends PageObject {
    @CacheLookup
    @FindBy(id = "ap_password")


    private WebElement passwordInputField;

    public AmazonPasswordLoginPage(WebDriver driver) {

    }

    @WhenPageOpens
    public void waitUntilAmazonLogoAppears() {

        $("/html/body/div[1]/div[1]/div[1]/div/a/i[1]").waitUntilVisible();

    }


}