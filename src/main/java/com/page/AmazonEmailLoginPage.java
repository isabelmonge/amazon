package com.page;


import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.WhenPageOpens;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;

@DefaultUrl("https://www.amazon.es/ap/signin?_encoding=UTF8&ignoreAuthState=1&openid.assoc_handle=esflex&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.mode=checkid_setup&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.ns.pape=http%3A%2F%2Fspecs.openid.net%2Fextensions%2Fpape%2F1.0&openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fwww.amazon.es%2Fgp%2Fcss%2Fhomepage.html%3Fie%3DUTF8%26%252AVersion%252A%3D1%26%252Aentries%252A%3D0%26ref_%3Dnav_signin&switch_account=\n")

public class AmazonEmailLoginPage extends PageObject {


        @CacheLookup
        @FindBy( id = "ap_mail")


        private WebElement emailInputField;

        public AmazonEmailLoginPage(WebDriver driver) {
            super(driver);
        }

        @WhenPageOpens
        public void waitUntilAmazonLogoAppears() {
            $("/html/body/div[1]/div[1]/div[1]/div/a/i[1]").waitUntilVisible();
        }


        public AmazonPasswordLoginPage fillPassword(String password) {
            element(emailInputField).clear();
            element(emailInputField).typeAndEnter(password);
            return new AmazonPasswordLoginPage(getDriver());
        }

}
